<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/','IndexController@index');


Route::get('/register', 'RegisterController@index')->name('register.index');

Route::group([
    'prefix' => 'aboutus',
    'as' => 'aboutus.',
], function (){
    Route::get('/', 'AboutUsController@index')->name('index');

});

Route::group([
    'prefix' => 'contactus',
    'as' => 'contactus.',
], function (){
    Route::get('/', 'ContactUsController@index')->name('index');

});
Route::group([
    'prefix' => 'product',
    'as' => 'product.',
], function (){
    Route::get('/{slug}', 'ProductController@index')->name('index');
    Route::post('/store', 'ProductController@store')->name('store');


});
//User dashboard
Route::group([
    'prefix' => 'user',
    'as' => 'user.',
    'namespace' => 'Dashboard'
], function (){
    Route::get('/', 'UserController@index')->name('index');
    Route::get('/settings', 'SettingsController@index')->name('settings');
    Route::delete('/delete/{id}', 'UserController@destroy')->name('destroy');
});

//Admin dashboard
Route::group([
    'prefix' => 'dashboard',
    'as' => 'dashboard.',
    'namespace' => 'Dashboard'
], function (){
    Route::get('/', 'AdminController@index')->name('index');
    Route::get('/user', 'AdminController@user')->name('user');
    Route::get('/user/edit/{id}', 'AdminController@edit')->name('edit');
    Route::get('/product', 'ProductController@index')->name('product');
    Route::post('/product/store', 'ProductController@store')->name('product.store');
    Route::delete('/product/delete/{id}', 'ProductController@destroy')->name('product.destroy');


    Route::get('/category', 'CategoryController@index')->name('category');
    Route::post('/category/save', 'CategoryController@save')->name('save');
    Route::delete('/category/delete/{id}', 'CategoryController@destroy')->name('category.destroy');
    Route::put('/category/update/{id}', 'CategoryController@update')->name('category.update');


    Route::get('/bidproduct', 'BidProductController@index')->name('bid');





});

Route::get('/home', 'HomeController@index')->name('home');
