<?php
if (! function_exists('isAdmin')) {
    function isAdmin($user) {
        return $user->role !== 'user' && $user->role !== 'admin' ;
    }
}
