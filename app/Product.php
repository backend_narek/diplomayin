<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;


class Product extends Model
{
    protected $fillable = [
        'title',
        'start_price',
        'price' ,
        'category_id',
        'slug',
        'user_id',
        'description',
        'short_description' ,
        'auction_time' ,
        'image'
    ];

    protected $with = ['category','user'];

   /* protected $appends = ['category','user'];

    public function getCategoryAttribute(){
        return $this->category()->first();
    }

    public function getUserAttribute(){
        return $this->user()->first();
    }*/

    public function category(){
        return $this->belongsTo(Category::class,'category_id','id');
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function bids(){
        return $this->hasMany(ProductBid::class,'product_id','id');
    }

    public function maxBid(){
        return $this->bids()->max('bid');
    }

    public function getAuctionTimeAttribute($value){
        $date = Carbon::make($value);
        return [
            'full' => $value,
            'year' => $date->year,
            'month' => $date->month,
            'day' => $date->day,
            'hour' => $date->hour,
            'minute' => $date->minute,
            'second' => $date->second
        ];
    }
}
