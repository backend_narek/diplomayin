<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;


class CategoryController extends Controller
{
    //
    public function index(){
        $categories = Category::orderBy('id', 'desc')->get();

        return view('dashboard.admin.category',compact('categories'));
    }

    public function save(Request $request){
        $validation =  $request->validate([
            'name' =>'required|unique:categories,name',
            ]);

        Category::create([
            'name' => $request->input('name')
        ]);

        return redirect()->route('dashboard.category')->with('status', 'Category is Added');
    }

    public function destroy($id){
        Category::query()->where('id',$id)->delete();
        return response()->json([
           'success' => true
        ]);
    }

    public function update(Request $request,$id){
        $category = Category::query()->where('id', $id)->firstOrFail();
        $category->name = $request->input('name');
        $category->update();
        return response()->json([
            'success'=> true
        ]);
    }
}
