<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\User;

class AdminController extends Controller
{
    //
    public function index(){
        return view('dashboard.user.index');
    }

    public function user(){
        $users = User::orderBy('id', 'desc')->get();
        return view('dashboard.admin.index',compact('users'));
    }

    public function edit(Request $request, $id){
        $user = User::findOrFail($id);
        return view('dashboard.admin.edit', compact('user'));
    }
}
