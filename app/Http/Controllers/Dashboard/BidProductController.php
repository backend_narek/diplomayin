<?php

namespace App\Http\Controllers\Dashboard;


use App\Product;
use App\ProductBid;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class BidProductController extends Controller
{
    //
    public function index(){
        $products = Product::query()->where('user_id',Auth::id())->with([
            'bids' => function($q){
                return $q->orderBy('id','desc')->take(10);
            }
        ])->get();





        return view('dashboard.admin.bidproduct', compact('products'));;
    }

}
