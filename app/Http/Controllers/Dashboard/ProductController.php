<?php

namespace App\Http\Controllers\Dashboard;

use App\Category;
use App\Http\Controllers\Controller;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Validator;

class ProductController extends Controller
{
    public function index(){

        $products = Product::query();

        if(!isAdmin(Auth::user())){
            $products->where('user_id',Auth::id());
        }

        $products = $products->orderBy('id', 'desc')->get();

        $categories = Category::orderBy('id', 'desc')->get();
        return view('dashboard.admin.product' , compact('categories', 'products'));
    }

    public function destroy($id){

        $product = Product::query()->where('id',$id)->first();

        @unlink(public_path('/uploads/'.$product->image));
        $product->delete();

        return response()->json([
            'success' => true
        ]);
    }

    public function store(Request $request){

        $slug = Str::slug($request->input('slug'));

        $validator = Validator::make($request->all(), [
            'title' => 'required|min:5',
            'start_price' => 'required|integer',
            'price' => 'required|integer',
            'category_id' => 'required',
            'slug' => 'required|unique:products,slug',
            'description' => 'required',
            'short_description' => 'required',
            'auction_time' => 'required',
            'image' => 'required',
        ]);

        if ($validator->fails()){
            return response()->json([
                'errors' => $validator->errors()
            ]);
        }

        $imageName = time().'.'.$request['image']->getClientOriginalExtension();
        $request['image']->move(public_path('/uploads'), $imageName);

        $product = Product::create([
            'title' => $request->title,
            'start_price' => $request->start_price,
            'price' => $request->price,
            'category_id' => $request->category_id,
            'user_id' => Auth::user()->id,
            'slug' => $slug,
            'description' => $request->description,
            'short_description' => $request->short_description,
            'auction_time' => $request->auction_time,
            'image' => $imageName,
        ]);

        $product->category;
        $product->user;

        return response()->json([
            'success' => true,
            'product' => $product
        ]);
    }

}
