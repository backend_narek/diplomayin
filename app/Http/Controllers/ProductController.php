<?php

namespace App\Http\Controllers;


use App\Product;
use App\ProductBid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    //
    public function index($slug){

        $product = Product::query()->where('slug',$slug)->with('bids')->firstOrFail();

        return view('product.index', compact('product', ));
    }


    public function store(Request $request){
        ProductBid::create([
            'user_id'=> Auth::user()->id,
            'product_id'=> $request->input('id'),
            'bid'=> $request->input('bid')
        ]);
        return response()->json([
            'success' => true,
        ]);
    }
}
