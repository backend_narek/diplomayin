<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class IndexController extends Controller {

    public function index(){

        $count = Product::query()->count();
        $daily = [];
        if($count >= 3){
            $daily = Product::all()->random(3);
        }

        return view('main.index',compact('daily'));
    }

}
