<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductBid extends Model
{
    //
    protected $fillable = [
        'user_id',
        'product_id',
        'bid',

    ];


    public function product(){
        return $this->belongsTo(Product::class,'product_id','id');
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }
    public function bids(){
        return $this->hasMany(ProductBid::class,'product_id','id');
    }

    public function maxBid(){
        return $this->bids()->max('bid');
    }
}
