@extends('layouts.dashboard')

@section('title')
    Category
@endsection()

@section('content')
    <div class="col-lg-12">
        <div class="main-card mb-3 card">
            <div class="card-header justify-content-between">
                <h5 class="card-title">Category table</h5>

                <button type="button" class="btn mr-2 mb-2 btn-primary create-category-btn" data-toggle="modal"
                        data-target=".create-category">Create Category
                </button>

            @push('modal')
                <!-- Small modal -->
                    <div class="modal fade create-category" tabindex="-1" role="dialog"
                         aria-labelledby="mySmallModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <form action="{{ route('dashboard.save') }}" method="post">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLongTitle">Create new category</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        @csrf
                                        <div class="position-relative form-group">
                                            <label for="exampleEmail" class="">Name</label>
                                            <input name="name" type="text" class="form-control"
                                                   placeholder="New category name" value="{{ old('name') }}">
                                            @error('name')
                                                <small class="form-text" style="color: red">{{ $message }}</small>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                        </button>
                                        <button type="submit" class="btn btn-primary">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- Modal -->
                @endpush
            </div>
            <div class="card-body">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <table class="mb-0 table">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th> Name</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($categories as $category)
                        <tr>
                            <td>{{ $category->id }}</td>
                            <td class="td-{{ $category->id }}">{{ $category->name }}</td>
                            <td>
                                <button type="button" class="btn mr-2 mb-2 btn-primary" data-toggle="modal" data-target=".category-edit{{ $category->id }}">
                                    Edit
                                </button>
                                @push('modal')
                                    <div class="modal fade category-edit{{ $category->id }}"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <input name="name" type="text" class="form-control edit-input" placeholder="New category name" value="{{ $category->name }}">                                          </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn btn-primary edit-category-btn" data-id="{{$category->id}}">Save changes</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endpush
                            </td>
                            <td>
                                <button type="button" class="btn btn-danger delete-category-btn" data-id="{{  $category->id }}">Delete</button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="{{ asset('assets/js/sweetalert.min.js') }}"></script>
    <script>
        $(function () {
            @error('name')
                $('.create-category-btn').click();
            @enderror



            $('.delete-category-btn').click(function () {
                const id = $(this).data('id');
                const $this = $(this);
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            url: '/dashboard/category/delete/'+id,
                            type:'delete',
                            dataType:'json',
                            success:function (res) {
                                if(res.success){
                                    Swal.fire(
                                        'Deleted!',
                                        'Your file has been deleted.',
                                        'success'
                                    )
                                    $this.parents('tr').remove()
                                }
                            }
                        })
                    }
                })
            })
        })
    </script>
    <script>
        $('.edit-category-btn').click(function () {
            const id = $(this).data('id');
            const $this = $(this);
            const name = $(this).parents('.modal-content').find('input.edit-input').val();

            $.ajax({
                url: '/dashboard/category/update/'+id,
                type:'put',
                data:{
                    name:name
                },
                dataType:'json',
                success:function (result) {
                    if(result.success){
                        $this.siblings().click();
                        $('.td-'+id).html(name);
                    }
                }
            })
        })
    </script>
@endpush

