@extends('layouts.dashboard')

@section('title')
    User
@endsection()

@section('content')
    <div class="col-lg-12">
        <div class="main-card mb-3 card">
            <div class="card-body"><h5 class="card-title">Simple table</h5>
                <table class="mb-0 table">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Middle Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        @if(isAdmin(Auth::user()) || Auth::user()->role == 'user' )
                        <th>Edit</th>
                        <th>Delete</th>
                         @endif
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->first_name }}</td>
                        <td>{{ $user->last_name }}</td>
                        <td>{{ $user->middle_name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->phone }}</td>
                        @if(isAdmin(Auth::user()) || Auth::user()->role == 'user' )
                        <td>
                            <a href="{{ route('dashboard.edit', ['id'=>$user->id]) }}" class="btn btn-success">Edit</a>
                        </td>
                        <td>

                            <button type="button"  class="btn btn-danger delete-user-btn" data-id="{{ $user->id }}">Delete</button>

                        </td>
                        @endif
                    </tr>
                      @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="{{ asset('assets/js/sweetalert.min.js') }}"></script>
    <script>
        $(function () {
            $('.delete-user-btn').click(function () {
                const id = $(this).data('id');
                const $this = $(this);
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            url: '/user/delete/'+id,
                            type:'delete',
                            dataType:'json',
                            success:function (res) {
                                if(res.success){
                                    Swal.fire(
                                        'Deleted!',
                                        'Your file has been deleted.',
                                        'success'
                                    )
                                    $this.parents('tr').remove()
                                }
                            }
                        })
                    }
                })
            })
        })
    </script>
@endpush
