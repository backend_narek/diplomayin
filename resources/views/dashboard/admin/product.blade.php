@extends('layouts.dashboard')

@section('link')
  <link href="{{ asset('assets/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
@endsection

@section('title')
  Products
@endsection()

@section('content')
  <div class="col-lg-12">
    <div class="main-card mb-3 card">
      <div class="card-body">
        <h5 class="card-title">Simple table</h5>
        <button type="button" class="btn mr-2 mb-2 btn-primary float-right" data-toggle="modal"
                data-target=".bd-example-modal-lg">Create Product
        </button>
        @push('modal')
          <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
               aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <form id="add-form" enctype="multipart/form-data">

                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Product</h5>
                  </div>
                  <div class="modal-body">
                    <div class="form-group col-md-12">
                      <label for="dtp_input1" class="control-label">Auction End time</label>
                      <input size="16" type="text" name="auction_time" value="2012-06-15 14:45" readonly class="form_datetime form-control">
                      <small class="alert-danger error-auction_time" style="display: none">error</small>
                    </div>
                    <div class="form-group col-md-12">
                      <label for="">Title</label>
                      <input type="text" name="title" class="form-control">
                      <small class="alert-danger error-title" style="display: none">error</small>
                    </div>
                    <div class="form-group col-md-12">
                      <label for="">Start Price</label>
                      <input type="text" name="start_price" class="form-control">
                      <small class="alert-danger error-start_price" style="display: none">error</small>
                    </div>
                    <div class="form-group col-md-12">
                      <label for="">Buy now Price</label>
                      <input type="text" name="price" class="form-control">
                      <small class="alert-danger error-price" style="display: none">error</small>
                    </div>
                    <div class="form-group col-md-12">
                      <label for="">Category</label>
                      <select class="mb-2 form-control" name="category_id">
                        <option></option>
                        @foreach($categories as $category)
                          <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                      </select>
                      <small class="alert-danger error-category_id" style="display: none">error</small>
                    </div>
                      <div class="form-group col-md-12">
                          <label for="">Slug</label>
                          <input type="text" name="slug" class="form-control">
                          <small class="alert-danger error-slug" style="display: none">error</small>
                      </div>
                    <div class="form-group col-md-12">
                      <label for="">Description</label>
                      <textarea id="editor1" type="text" name="description" class="form-control"></textarea>
                      <small class="alert-danger error-description" style="display: none">error</small>
                    </div>
                    <div class="form-group col-md-12">
                      <label for="">ShortDescription</label>
                      <textarea id="editor2" type="text" name="short_description" class="form-control"></textarea>
                      <small class="alert-danger error-short_description" style="display: none">error</small>
                    </div>
                    <div class="form-group col-md-12">
                      <label for="">Choose image</label>
                      <input type="file" name="image" class="form-control">
                      <small class="alert-danger error-image" style="display: none">error</small>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary close_btn" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Add new product</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        @endpush

        <div>
          <table class="mb-0 table">
            <thead>
            <tr>
              <th>Id</th>
              <th>Title</th>
              <th>Start Price</th>
              <th>Buy Now Price</th>
              <th>Category</th>
              <th>User_id</th>
              <th>Slug</th>
              <th>Description</th>
              <th>ShortDescription</th>
              <th>Image</th>
              <th>Auction Time</th>
                @if(isAdmin(Auth::user()) || Auth::user()->role == 'user' )
              <th>Edit</th>
              <th>Delete</th>
                 @endif
            </tr>
            </thead>
            <tbody>
            @foreach($products as $product)

              <tr>
                <td>{{ $product->id }}</td>
                <td>{{ $product->title }}</td>
                <td>${{ $product->start_price }}</td>
                <td>${{ $product->price }}</td>
                <td>{{ $product->category->name }}</td>
                <td>{{ $product->user->first_name }}</td>
                <td>{{ $product->slug }}</td>
                <td>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".modal-description{{ $product->id }}">Description</button>
                </td>
                <td>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".modal-shortdescription{{ $product->id }}">ShortDescription</button>
                </td>
                <td>
                    <img src="{{ asset('uploads/'.$product->image) }}" style="width: 50px; height: 50px; border-radius: 50%;" alt="">
                    </td>
                <td>{{ $product->auction_time['full'] }}</td>
                  @if(isAdmin(Auth::user()) || Auth::user()->role == 'user' )
                <td>
                  <button href="" class="btn btn-success">Edit</button>
                </td>
                <td>
                    <button type="button" class="btn btn-danger delete-product-btn" data-id="{{ $product->id }}">Delete</button>
                </td>
                   @endif
              </tr>

              @push('modal')
                  <!-- Large modal description -->

                  <div class="modal fade modal-description{{ $product->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                              <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLongTitle">Description</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                  </button>
                              </div>
                              <div class="modal-body">
                                  {!! $product->description !!}
                              </div>
                          </div>
                      </div>
                  </div>
                  <!-- Large modal shortdescription -->

                  <div class="modal fade  modal-shortdescription{{ $product->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                              <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLongTitle">Description</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                  </button>
                              </div>
                              <div class="modal-body">
                                  {!! $product->short_description !!}
                              </div>
                          </div>
                      </div>
                  </div>

              @endpush

            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('script')
  <script type="text/javascript" src="{{ asset('assets/ckeditor/ckeditor.js') }}"></script>
  <script type="text/javascript" src="{{ asset('assets/js/moment-with-locales.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('assets/js/bootstrap-datetimepicker.min.js') }}"></script>
  <script src="{{ asset('assets/js/sweetalert.min.js') }}"></script>
  <script>
    CKEDITOR.replace('editor1')
    CKEDITOR.replace('editor2')
  </script>
  <script type="text/javascript">
    $(function () {



      $(".form_datetime").datetimepicker({
        format: 'yyyy-mm-dd hh:ii',
        autoclose: true
      });

      $('#add-form').submit(function (e) {
        e.preventDefault();
        const $this = $(this);
        const form = new FormData($this[0]);

        form.set('description',CKEDITOR.instances.editor1.getData());
        form.set('short_description',CKEDITOR.instances.editor2.getData());

        $.ajax({
          url:'/dashboard/product/store',
          type:'post',
          data:form,
          dataType:'json',
          contentType : false,
          processData : false,
          success:function (res) {
            if(res.errors)
            {
              $.each(res.errors, function(key, value){
                $('.error-'+key).show().html(value);
              });
            }else{
              const product = res.product;
              const html = `<tr>
                              <td>${product.id}</td>
                              <td>${product.title}</td>
                              <td>${product.start_price}</td>
                              <td>${product.price}</td>
                              <td>${product.category.name}</td>
                              <td>${product.user.first_name}</td>
                              <td>${product.slug}</td>
                              <td>
                                  <button type="button" class="btn btn-primary" data-toggle="modal"  data-target=".modal-description${product.id}">Description</button>
                              </td>
                              <td>
                                  <button type="button" class="btn btn-primary" data-toggle="modal"  data-target=".modal-shortdescription${product.id}">ShortDescription</button>
                              </td>
                              <td>
                               <img src="/uploads/${product.image}" style="width: 50px; height: 50px; border-radius: 50%;"></td>
                              <td>${product.auction_time.full}</td>
                              <td>
                                <button class="btn btn-success">Edit</button>
                              </td>
                              <td>

                                  <button type="button" class="btn btn-danger delete-product-btn" data-id="${ product.id }">Delete</button>

                              </td>
                          </tr>`;

              $('table tbody').prepend(html);
              $this.find('button[type=button]').click();
              $this.reset();
              CKEDITOR.instances.editor1.setData("");
              CKEDITOR.instances.editor2.setData("");
            }
          }
        })
      })
        $('.delete-product-btn').click(function () {
            const id = $(this).data('id');
            const $this = $(this);
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: '/dashboard/product/delete/'+id,
                        type:'delete',
                        dataType:'json',
                        success:function (res) {
                            if(res.success){
                                Swal.fire(
                                    'Deleted!',
                                    'Your file has been deleted.',
                                    'success'
                                )
                                $this.parents('tr').remove()
                            }
                        }
                    })
                }
            })
        })
    })
  </script>





@endpush
