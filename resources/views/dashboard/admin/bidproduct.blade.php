@extends('layouts.dashboard')

@section('title')
    Bid Products
@endsection()

@section('content')
    <div class="col-lg-12">
        <div class="main-card mb-3 card">
            <div class="card-body"><h5 class="card-title">Bid History</h5>
                <table class="mb-0 table">
                    <thead>
                    <tr>
                        <th>Username </th>
                        <th>Product Title</th>
                        <th>Bid</th>
                        <th>Bid Time</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                            @foreach($product->bids as $item)
                        <tr>
                            <td>{{ $item->user->first_name }}</td>
                            <td>{{ $product->title }}</td>
                            <td>${{ $item->bid }}</td>
                            <td>{{ $item->created_at }}</td>
                        </tr>
                       @endforeach
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('script')

@endpush

