@extends('layouts.dashboard')

@section('title')
    Account Setting
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="main-card mb-3 card">
                <div class="card-body"><h5 class="card-title">Controls Types</h5>
                    <form class="">


                        <div class="position-relative form-group"><label  class="">First Name</label><input
                                name="first_name" type="text"
                                class="form-control" value="{{ $user->first_name }}">
                        </div>
                        <div class="position-relative form-group"><label class="">Email</label><input
                                name="email"  type="email"
                                class="form-control" value="{{ $user->email }}">
                        </div>
                        <div class="position-relative form-group"><label  class="">Middle Name</label><input
                                name="middle_name"  type="text"
                                class="form-control" value="{{ $user->middle_name }}">
                        </div>
                        <div class="position-relative form-group"><label class="">Last name</label><input
                                name="last_name" id="examplePassword" type="text"
                                class="form-control" value="{{ $user->last_name }}">
                        </div>
                        <div class="position-relative form-group"><label class="">Phone</label><input
                                name="phone" id="examplePassword" type="text"
                                class="form-control" value="{{ $user->phone }}">
                        </div>
                        <div class="position-relative form-group"><label  class="">Password</label><input
                                name="password"  type="password"
                                class="form-control" >
                        </div>
                        <div class="position-relative form-group"><label  class=""> Click to download this file</label>
                            <a href="{{ asset('uploads/'.$user->file) }}" download>{{ $user->file }}</a>
                        </div>


                        <button class="mt-1 btn btn-primary">update</button>
                        <a href="{{ route('dashboard.user') }}" class="mt-1 btn btn-danger">Cancel</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')

@endpush
