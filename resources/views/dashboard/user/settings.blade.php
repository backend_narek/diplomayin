@extends('layouts.dashboard')

@section('title')
    Account Setting
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="main-card mb-3 card">
                <div class="card-body"><h5 class="card-title">Controls Types</h5>
                    <form class="">
                        <div class="position-relative form-group"><label class="">Email</label><input
                                name="email"  type="email"
                                class="form-control" value="{{ Auth::user()->email }}">
                        </div>
                        <div class="position-relative form-group"><label  class="">Password</label><input
                                name="password"  type="password"
                                class="form-control" >
                        </div>
                        <div class="position-relative form-group"><label  class="">First Name</label><input
                                name="first_name" type="text"
                                class="form-control" value="{{ Auth::user()->first_name }}">
                        </div>
                        <div class="position-relative form-group"><label  class="">Middle Name</label><input
                                name="middle_name"  type="text"
                                class="form-control" value="{{ Auth::user()->middle_name }}">
                        </div>
                        <div class="position-relative form-group"><label class="">Last name</label><input
                                name="last_name" id="examplePassword" type="text"
                                class="form-control" value="{{ Auth::user()->last_name }}">
                        </div>

                        <div class="position-relative form-group"><label for="exampleFile" class="">File</label><input
                                name="file" id="exampleFile" type="file" class="form-control-file">
                            <small class="form-text text-muted">This is some placeholder block-level help text for the
                                above input. It's a bit lighter and easily wraps to a new line.</small>
                        </div>
                        <button class="mt-1 btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')

@endpush()

