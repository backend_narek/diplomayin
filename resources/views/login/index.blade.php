@extends('layouts.app')

@section('content')
    <main class="site-main site-login">
        <div class="container">
            <ol class="breadcrumb-page">
                <li><a href="index.html">Home </a></li>
                <li class="active"><a href="#">Login</a></li>
            </ol>
        </div>
        <div class="customer-login">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <h5 class="title-login">Log in to your account</h5>
                        <p class="p-title-login">Wellcome to your account.</p>
                        <form class="login" method="post" action="login">
                            @csrf
                            <p class="form-row form-row-wide">
                                <label>Username or Email Address:<span class="required"></span></label>
                                <input type="text" value="" name="email"
                                       placeholder="Type your username or email address" class="input-text">
                            </p>
                            <p class="form-row form-row-wide">
                                <label>Password:<span class="required"></span></label>
                                <input type="password" name="password" placeholder="************" class="input-text">
                            </p>
                            <ul class="inline-block">
                                <li><label class="inline"><input type="checkbox"><span class="input"></span>Remember me</label>
                                </li>
                            </ul>
                            <a href="#" class="forgot-password">Forgotten password?</a>
                            <p class="form-row">
                                <input type="submit" value="Login" name="Login" class="button-submit">
                            </p>
                        </form>
                    </div>
                    <div class="col-sm-6 border-after">
                        <h5 class="title-login">Great an account</h5>
                        <p class="p-title-login">Fersonal infomation</p>
                        <form class="register" method="post" action="register" enctype="multipart/form-data">
                            @csrf

                            <p class="form-row form-row-wide col-md-6 padding-left">
                                <label>First name<span class="required">*</span></label>
                                <input type="text" value="{{ old('first_name') }}" name="first_name" placeholder="First name" class="input-text @error('first_name') is-invalid @enderror">

                                @error('first_name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </p>
                            <p class="form-row form-row-wide col-md-6 padding-right">
                                <label>Middle Name/Initial<span class="required"></span></label>
                                <input title="midname" type="text" value="{{ old('middle_name') }}" name="middle_name" class="input-text @error('first_name') is-invalid @enderror">
                                @error('middle_name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </p>
                            <p class="form-row form-row-wide">
                                <label>Last name<span class="required">*</span></label>
                                <input title="lastname" type="text" value="{{ old('last_name') }}" name="last_name" placeholder="Last name" class="input-text">
                            </p>
                            <p class="form-row form-row-wide">
                                <label>Email Address<span class="required">*</span></label>
                                <input title="email" type="email" value="{{ old('email') }}" name="email" placeholder="Email address" class="input-text">
                            </p>
                            <p class="form-row form-row-wide">
                                <label>Phone Number<span class="required">*</span></label>
                                <input title="phone" type="text" alue="{{ old('phone') }}" name="phone" placeholder="Phone number" class="input-text">
                            </p>

                            <p class="form-row form-row-wide col-md-6 padding-left">
                                <label>Password:<span class="required"></span></label>
                                <input title="pass" type="password"  name="password" class="input-text">
                            </p>
                            <p class="form-row form-row-wide col-md-6 padding-right">
                                <label>Confirm Password<span class="required">*</span></label>
                                <input title="pass2" type="password" name="password_confirmation" class="input-text">
                            </p>
                            <p class="form-row form-row-wide">
                                <label>Choose File<span class="required">*</span></label>
                                <input type="file" name="file" class="filestyle">
                            </p>
                            <ul>
                                <li><label class="inline">
                                        <input type="checkbox" name="newsletter">
                                        <span class="input" ></span>Sign Up for
                                        Newsletter</label></li>
                            </ul>
                            <p class="form-row">
                                <input type="submit" value="Submit" name="Submit" class="button-submit">
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('script')
    <script>
        $(":file").filestyle();
    </script>
@endsection
