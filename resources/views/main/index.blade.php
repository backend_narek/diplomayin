@extends('layouts.app')



@section('content')
    <main class="site-main">
        <div class="block-section-1">
            <div class="main-slide slide-opt-1 full-width">
                <div class="owl-carousel nav-style1" data-nav="false" data-autoplay="false" data-dots="true"
                     data-loop="true" data-margin="0"
                     data-responsive='{"0":{"items":1},"600":{"items":1},"1000":{"items":1}}'>
                    <div class="item-slide item-slide-1">
                        <div class="slide-desc slide-desc-1">
                            <div class="container">
                                <div class="container-inner">
                                    <div class="p-primary">Discovery And Explore Digital</div>
                                    <p>Vehicula curae mi senectus sodales proin, ultricies gravida.</p>
                                    <a href="grid-product.html" class="btn-shop-now">Shop Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item-slide item-slide-2">
                        <div class="slide-desc slide-desc-2">
                            <div class="container">
                                <div class="container-inner">
                                    <div class="p-primary">Smart Watches New Collection</div>
                                    <p>Inceptos aptent eleifend metus. Porta montes nibh mollis</p>
                                    <a href="grid-product.html" class="btn-shop-now">Shop Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item-slide item-slide-3">
                        <div class="slide-desc slide-desc-3">
                            <div class="container">
                                <div class="container-inner">
                                    <div class="p-primary">Follow Keeping through Control</div>
                                    <p>Lorem ipsum dolor sit amet consectetur, adipiscing turpis</p>
                                    <a href="grid-product.html" class="btn-shop-now">Shop Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="block-section-99">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="dagon-iconbox style-02">
                            <div class="iconbox-inner">
                                <div class="icon"><span class="flaticon-delivery-truck"></span></div>
                                <div class="content"><h4 class="title">FAST SHIPPING</h4>
                                    <p class="text">Dispatch on most items</p></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="dagon-iconbox style-02">
                            <div class="iconbox-inner">
                                <div class="icon"><span class="flaticon-refresh"></span></div>
                                <div class="content"><h4 class="title">30 DAY RETURns</h4>
                                    <p class="text">For peace of mind</p></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="dagon-iconbox style-02">
                            <div class="iconbox-inner">
                                <div class="icon"><span class="flaticon-quality-badge"></span></div>
                                <div class="content"><h4 class="title">BEST FURNITURE</h4>
                                    <p class="text">Online retailer for home</p></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if(count($daily) >= 3)
            <div class="block-daily-deals style1">
                <div class="container">
                    <div class="title-of-section main-title">Daily deals</div>
                    <div class="owl-carousel nav-style2 border-background equal-container" data-nav="true"
                         data-autoplay="false" data-dots="false" data-loop="true" data-margin="30"
                         data-responsive='{"0":{"items":1},"530":{"items":2},"768":{"items":3},"992":{"items":2}}'>

                        @foreach($daily as $item)
                            <div class="deal-of-day equal-elem">
                                <div class="product-thumb style1">
                                    <div class="thumb-inner">
                                        <a href="#"><img src="{{ asset('uploads/'.$item->image) }}"  alt="d1"></a>
                                    </div>
                                </div>
                                <div class="product-innfo">
                                    <div class="product-name"><a href="{{ route('product.index',$item->slug)  }}">{{ $item->title }}</a></div>
                                    <span class="price">
                                    <ins>${{ $item->start_price }}</ins>
                                    <small>${{ $item->price }}</small>
                                    <span class="onsale">-50%</span>
                                </span>
                                    <span class="star-rating">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <span class="review">5 Review(s)</span>
                                </span>

                                    <div class="product-count-down">
                                        <div class="kt-countdown"
                                             data-y="{{ $item->auction_time['year'] }}"
                                             data-m="{{ $item->auction_time['month'] }}"
                                             data-d="{{ $item->auction_time['day'] }}"
                                             data-h="{{ $item->auction_time['hour'] }}"
                                             data-i="{{ $item->auction_time['minute'] }}"
                                             data-s="{{ $item->auction_time['second'] }}"></div>
                                    </div>
                                    <a href="#" class="btn-add-to-cart">Bid Now</a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
        <div class="block-section-3">
            <div class="container">
                <div class="row">
                    <div class="col-md-5 col-sm-5">
                        <div class="promotion-banner style-1">
                            <a href="#" class="banner-img">
                                <img src="assets/images/home2/banner-1.jpg" alt="banner-1">
                            </a>
                            <div class="promotion-banner-inner">
                                <h4>Headphone</h4>
                                <h3>Sale <strong>40%</strong> Off</h3>
                                <a class="banner-link" href="grid-product.html">Shop now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7 col-sm-7">
                        <div class="promotion-banner style-1">
                            <a href="#" class="banner-img"><img src="assets/images/home2/banner-2.jpg"
                                                                alt="banner-2"></a>
                            <div class="promotion-banner-inner">
                                <h4>New Arrivals</h4>
                                <h3>Get <strong>25%</strong> Flat Off</h3>
                                <a class="banner-link" href="grid-product.html">Shop now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="block-section-4">
            <div class="container">
                <div class="title-of-section main-title">Featured Products</div>
                <div class="tab-product tab-product-fade-effect">
                    <ul class="box-tabs nav-tab">
                        <li class="active"><a data-animated="" data-toggle="tab" href="#tab-1">All Products</a></li>
                        <li><a data-animated="" data-toggle="tab" href="#tab-2">Camera</a></li>
                        <li><a data-animated="" data-toggle="tab" href="#tab-3">Laptop</a></li>
                        <li><a data-animated="" data-toggle="tab" href="#tab-4">Consoles</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-container">
                            <div id="tab-1" class="tab-panel active">
                                <div class="owl-carousel nav-style2 border-background equal-container" data-nav="false"
                                     data-autoplay="false" data-dots="false" data-loop="true" data-margin="30"
                                     data-responsive='{"0":{"items":1},"480":{"items":2},"768":{"items":3},"992":{"items":4},"1200":{"items":4}}'>
                                    <div class="owl-one-row">
                                        <div class="product-item style1">
                                            <div class="product-inner equal-elem">
                                                <div class="product-thumb">
                                                    <div class="thumb-inner">
                                                        <a href="#"><img src="assets/images/home2/p1.jpg" alt="p1"></a>
                                                    </div>
                                                    <span class="onsale">-50%</span>
                                                    <a href="#" class="quick-view">Quick View</a>
                                                </div>
                                                <div class="product-innfo">
                                                    <div class="product-name"><a href="#">Game Controller
                                                        </a></div>
                                                    <span class="price">
                                                            <ins>$229.00</ins>
                                                            <del>$259.00</del>
                                                        </span>
                                                    <span class="star-rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <span class="review">5 Review(s)</span>
                                                        </span>
                                                    <div class="group-btn-hover">
                                                        <div class="inner">
                                                            <a href="compare.html" class="compare"><i
                                                                    class="fa fa-exchange"></i></a>
                                                            <a href="#" class="add-to-cart">Add to cart</a>
                                                            <a href="wishlist.html" class="wishlist"><i
                                                                    class="fa fa-heart-o"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-item style1">
                                            <div class="product-inner equal-elem">
                                                <div class="product-thumb">
                                                    <div class="thumb-inner">
                                                        <a href="#"><img src="assets/images/home2/p6.jpg" alt="p6"></a>
                                                    </div>
                                                    <span class="onsale">-50%</span>
                                                    <a href="#" class="quick-view">Quick View</a>
                                                </div>
                                                <div class="product-innfo">
                                                    <div class="product-name"><a href="#">Retina Laptop
                                                        </a></div>
                                                    <span class="price price-dark">
                                                            <ins>$229.00</ins>
                                                        </span>
                                                    <span class="star-rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <span class="review">5 Review(s)</span>
                                                        </span>
                                                    <div class="group-btn-hover">
                                                        <div class="inner">
                                                            <a href="compare.html" class="compare"><i
                                                                    class="fa fa-exchange"></i></a>
                                                            <a href="#" class="add-to-cart">Add to cart</a>
                                                            <a href="wishlist.html" class="wishlist"><i
                                                                    class="fa fa-heart-o"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="owl-one-row">
                                        <div class="product-item style1">
                                            <div class="product-inner equal-elem">
                                                <div class="product-thumb">
                                                    <div class="thumb-inner">
                                                        <a href="#"><img src="assets/images/home2/p2.jpg" alt="p2"></a>
                                                    </div>
                                                    <span class="onsale">-50%</span>
                                                    <a href="#" class="quick-view">Quick View</a>
                                                </div>
                                                <div class="product-innfo">
                                                    <div class="product-name"><a href="#">Photo Camera
                                                        </a></div>
                                                    <span class="price">
                                                            <ins>$229.00</ins>
                                                            <del>$259.00</del>
                                                        </span>
                                                    <span class="star-rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <span class="review">5 Review(s)</span>
                                                        </span>
                                                    <div class="group-btn-hover">
                                                        <div class="inner">
                                                            <a href="compare.html" class="compare"><i
                                                                    class="fa fa-exchange"></i></a>
                                                            <a href="#" class="add-to-cart">Add to cart</a>
                                                            <a href="wishlist.html" class="wishlist"><i
                                                                    class="fa fa-heart-o"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-item style1">
                                            <div class="product-inner equal-elem">
                                                <div class="product-thumb">
                                                    <div class="thumb-inner">
                                                        <a href="#"><img src="assets/images/home2/p7.jpg" alt="p7"></a>
                                                    </div>
                                                    <a href="#" class="quick-view">Quick View</a>
                                                </div>
                                                <div class="product-innfo">
                                                    <div class="product-name"><a href="#">Ring Video Doorbell
                                                        </a></div>
                                                    <span class="price price-dark">
                                                            <ins>$229.00</ins>
                                                        </span>
                                                    <span class="star-rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <span class="review">5 Review(s)</span>
                                                        </span>
                                                    <div class="group-btn-hover">
                                                        <div class="inner">
                                                            <a href="compare.html" class="compare"><i
                                                                    class="fa fa-exchange"></i></a>
                                                            <a href="#" class="add-to-cart">Add to cart</a>
                                                            <a href="wishlist.html" class="wishlist"><i
                                                                    class="fa fa-heart-o"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="owl-one-row">
                                        <div class="product-item style1">
                                            <div class="product-inner equal-elem">
                                                <div class="product-thumb">
                                                    <div class="thumb-inner">
                                                        <a href="#"><img src="assets/images/home2/p3.jpg" alt="p3"></a>
                                                    </div>
                                                    <span class="onnew">new</span>
                                                    <a href="#" class="quick-view">Quick View</a>
                                                </div>
                                                <div class="product-innfo">
                                                    <div class="product-name"><a href="#">Smartphone 4 GB
                                                        </a></div>
                                                    <span class="price price-dark">
                                                            <ins>$229.00</ins>
                                                        </span>
                                                    <span class="star-rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <span class="review">5 Review(s)</span>
                                                        </span>
                                                    <div class="group-btn-hover">
                                                        <div class="inner">
                                                            <a href="compare.html" class="compare"><i
                                                                    class="fa fa-exchange"></i></a>
                                                            <a href="#" class="add-to-cart">Add to cart</a>
                                                            <a href="wishlist.html" class="wishlist"><i
                                                                    class="fa fa-heart-o"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-item style1">
                                            <div class="product-inner equal-elem">
                                                <div class="product-thumb">
                                                    <div class="thumb-inner">
                                                        <a href="#"><img src="assets/images/home2/p8.jpg" alt="p8"></a>
                                                    </div>
                                                    <span class="onsale">-50%</span>
                                                    <a href="#" class="quick-view">Quick View</a>
                                                </div>
                                                <div class="product-innfo">
                                                    <div class="product-name"><a href="#">Retina Laptop
                                                        </a></div>
                                                    <span class="price">
                                                            <ins>$229.00</ins>
                                                            <del>$259.00</del>
                                                        </span>
                                                    <span class="star-rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <span class="review">5 Review(s)</span>
                                                        </span>
                                                    <div class="group-btn-hover">
                                                        <div class="inner">
                                                            <a href="compare.html" class="compare"><i
                                                                    class="fa fa-exchange"></i></a>
                                                            <a href="#" class="add-to-cart">Add to cart</a>
                                                            <a href="wishlist.html" class="wishlist"><i
                                                                    class="fa fa-heart-o"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="owl-one-row">
                                        <div class="product-item style1">
                                            <div class="product-inner equal-elem">
                                                <div class="product-thumb">
                                                    <div class="thumb-inner">
                                                        <a href="#"><img src="assets/images/home2/p4.jpg" alt="p4"></a>
                                                    </div>
                                                    <a href="#" class="quick-view">Quick View</a>
                                                </div>
                                                <div class="product-innfo">
                                                    <div class="product-name"><a href="#">Smartphone New
                                                        </a></div>
                                                    <span class="price price-dark">
                                                            <ins>$229.00</ins>
                                                        </span>
                                                    <span class="star-rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <span class="review">5 Review(s)</span>
                                                        </span>
                                                    <div class="group-btn-hover">
                                                        <div class="inner">
                                                            <a href="compare.html" class="compare"><i
                                                                    class="fa fa-exchange"></i></a>
                                                            <a href="#" class="add-to-cart">Add to cart</a>
                                                            <a href="wishlist.html" class="wishlist"><i
                                                                    class="fa fa-heart-o"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-item style1">
                                            <div class="product-inner equal-elem">
                                                <div class="product-thumb">
                                                    <div class="thumb-inner">
                                                        <a href="#"><img src="assets/images/home2/p9.jpg" alt="p9"></a>
                                                    </div>
                                                    <a href="#" class="quick-view">Quick View</a>
                                                </div>
                                                <div class="product-innfo">
                                                    <div class="product-name"><a href="#">Smartphone New
                                                        </a></div>
                                                    <span class="price price-dark">
                                                            <ins>$229.00</ins>
                                                        </span>
                                                    <span class="star-rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <span class="review">5 Review(s)</span>
                                                        </span>
                                                    <div class="group-btn-hover">
                                                        <div class="inner">
                                                            <a href="compare.html" class="compare"><i
                                                                    class="fa fa-exchange"></i></a>
                                                            <a href="#" class="add-to-cart">Add to cart</a>
                                                            <a href="wishlist.html" class="wishlist"><i
                                                                    class="fa fa-heart-o"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="owl-one-row">
                                        <div class="product-item style1">
                                            <div class="product-inner equal-elem">
                                                <div class="product-thumb">
                                                    <div class="thumb-inner">
                                                        <a href="#"><img src="assets/images/home2/p5.jpg" alt="p5"></a>
                                                    </div>
                                                    <a href="#" class="quick-view">Quick View</a>
                                                </div>
                                                <div class="product-innfo">
                                                    <div class="product-name"><a href="#">Notebook Pro
                                                        </a></div>
                                                    <span class="price price-dark">
                                                            <ins>$229.00</ins>
                                                        </span>
                                                    <span class="star-rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <span class="review">5 Review(s)</span>
                                                        </span>
                                                    <div class="group-btn-hover">
                                                        <div class="inner">
                                                            <a href="compare.html" class="compare"><i
                                                                    class="fa fa-exchange"></i></a>
                                                            <a href="#" class="add-to-cart">Add to cart</a>
                                                            <a href="wishlist.html" class="wishlist"><i
                                                                    class="fa fa-heart-o"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-item style1">
                                            <div class="product-inner equal-elem">
                                                <div class="product-thumb">
                                                    <div class="thumb-inner">
                                                        <a href="#"><img src="assets/images/home2/p10.jpg"
                                                                         alt="p10"></a>
                                                    </div>
                                                    <span class="onsale">-50%</span>
                                                    <a href="#" class="quick-view">Quick View</a>
                                                </div>
                                                <div class="product-innfo">
                                                    <div class="product-name"><a href="#">Notebook Pro
                                                        </a></div>
                                                    <span class="price">
                                                            <ins>$229.00</ins>
                                                            <del>$259.00</del>
                                                        </span>
                                                    <span class="star-rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <span class="review">5 Review(s)</span>
                                                        </span>
                                                    <div class="group-btn-hover">
                                                        <div class="inner">
                                                            <a href="compare.html" class="compare"><i
                                                                    class="fa fa-exchange"></i></a>
                                                            <a href="#" class="add-to-cart">Add to cart</a>
                                                            <a href="wishlist.html" class="wishlist"><i
                                                                    class="fa fa-heart-o"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-2" class="tab-panel">
                                <div class="owl-carousel nav-style2 border-background equal-container" data-nav="false"
                                     data-autoplay="false" data-dots="false" data-loop="true" data-margin="30"
                                     data-responsive='{"0":{"items":1},"480":{"items":2},"768":{"items":3},"992":{"items":4},"1200":{"items":4}}'>
                                    <div class="owl-one-row">
                                        <div class="product-item style1">
                                            <div class="product-inner equal-elem">
                                                <div class="product-thumb">
                                                    <div class="thumb-inner">
                                                        <a href="#"><img src="assets/images/home2/p6.jpg" alt="p6"></a>
                                                    </div>
                                                    <span class="onsale">-50%</span>
                                                    <a href="#" class="quick-view">Quick View</a>
                                                </div>
                                                <div class="product-innfo">
                                                    <div class="product-name"><a href="#">Retina Laptop
                                                        </a></div>
                                                    <span class="price price-dark">
                                                            <ins>$229.00</ins>
                                                        </span>
                                                    <span class="star-rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <span class="review">5 Review(s)</span>
                                                        </span>
                                                    <div class="group-btn-hover">
                                                        <div class="inner">
                                                            <a href="compare.html" class="compare"><i
                                                                    class="fa fa-exchange"></i></a>
                                                            <a href="#" class="add-to-cart">Add to cart</a>
                                                            <a href="wishlist.html" class="wishlist"><i
                                                                    class="fa fa-heart-o"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-item style1">
                                            <div class="product-inner equal-elem">
                                                <div class="product-thumb">
                                                    <div class="thumb-inner">
                                                        <a href="#"><img src="assets/images/home2/p2.jpg" alt="p2"></a>
                                                    </div>
                                                    <span class="onsale">-50%</span>
                                                    <a href="#" class="quick-view">Quick View</a>
                                                </div>
                                                <div class="product-innfo">
                                                    <div class="product-name"><a href="#">Photo Camera
                                                        </a></div>
                                                    <span class="price">
                                                            <ins>$229.00</ins>
                                                            <del>$259.00</del>
                                                        </span>
                                                    <span class="star-rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <span class="review">5 Review(s)</span>
                                                        </span>
                                                    <div class="group-btn-hover">
                                                        <div class="inner">
                                                            <a href="compare.html" class="compare"><i
                                                                    class="fa fa-exchange"></i></a>
                                                            <a href="#" class="add-to-cart">Add to cart</a>
                                                            <a href="wishlist.html" class="wishlist"><i
                                                                    class="fa fa-heart-o"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="owl-one-row">
                                        <div class="product-item style1">
                                            <div class="product-inner equal-elem">
                                                <div class="product-thumb">
                                                    <div class="thumb-inner">
                                                        <a href="#"><img src="assets/images/home2/p7.jpg" alt="p7"></a>
                                                    </div>
                                                    <a href="#" class="quick-view">Quick View</a>
                                                </div>
                                                <div class="product-innfo">
                                                    <div class="product-name"><a href="#">Ring Video Doorbell
                                                        </a></div>
                                                    <span class="price price-dark">
                                                            <ins>$229.00</ins>
                                                        </span>
                                                    <span class="star-rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <span class="review">5 Review(s)</span>
                                                        </span>
                                                    <div class="group-btn-hover">
                                                        <div class="inner">
                                                            <a href="compare.html" class="compare"><i
                                                                    class="fa fa-exchange"></i></a>
                                                            <a href="#" class="add-to-cart">Add to cart</a>
                                                            <a href="wishlist.html" class="wishlist"><i
                                                                    class="fa fa-heart-o"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-item style1">
                                            <div class="product-inner equal-elem">
                                                <div class="product-thumb">
                                                    <div class="thumb-inner">
                                                        <a href="#"><img src="assets/images/home2/p1.jpg" alt="p1"></a>
                                                    </div>
                                                    <span class="onsale">-50%</span>
                                                    <a href="#" class="quick-view">Quick View</a>
                                                </div>
                                                <div class="product-innfo">
                                                    <div class="product-name"><a href="#">Game Controller
                                                        </a></div>
                                                    <span class="price">
                                                            <ins>$229.00</ins>
                                                            <del>$259.00</del>
                                                        </span>
                                                    <span class="star-rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <span class="review">5 Review(s)</span>
                                                        </span>
                                                    <div class="group-btn-hover">
                                                        <div class="inner">
                                                            <a href="compare.html" class="compare"><i
                                                                    class="fa fa-exchange"></i></a>
                                                            <a href="#" class="add-to-cart">Add to cart</a>
                                                            <a href="wishlist.html" class="wishlist"><i
                                                                    class="fa fa-heart-o"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="owl-one-row">
                                        <div class="product-item style1">
                                            <div class="product-inner equal-elem">
                                                <div class="product-thumb">
                                                    <div class="thumb-inner">
                                                        <a href="#"><img src="assets/images/home2/p8.jpg" alt="p8"></a>
                                                    </div>
                                                    <span class="onsale">-50%</span>
                                                    <a href="#" class="quick-view">Quick View</a>
                                                </div>
                                                <div class="product-innfo">
                                                    <div class="product-name"><a href="#">Retina Laptop
                                                        </a></div>
                                                    <span class="price">
                                                            <ins>$229.00</ins>
                                                            <del>$259.00</del>
                                                        </span>
                                                    <span class="star-rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <span class="review">5 Review(s)</span>
                                                        </span>
                                                    <div class="group-btn-hover">
                                                        <div class="inner">
                                                            <a href="compare.html" class="compare"><i
                                                                    class="fa fa-exchange"></i></a>
                                                            <a href="#" class="add-to-cart">Add to cart</a>
                                                            <a href="wishlist.html" class="wishlist"><i
                                                                    class="fa fa-heart-o"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-item style1">
                                            <div class="product-inner equal-elem">
                                                <div class="product-thumb">
                                                    <div class="thumb-inner">
                                                        <a href="#"><img src="assets/images/home2/p4.jpg" alt="p4"></a>
                                                    </div>
                                                    <a href="#" class="quick-view">Quick View</a>
                                                </div>
                                                <div class="product-innfo">
                                                    <div class="product-name"><a href="#">Smartphone New
                                                        </a></div>
                                                    <span class="price price-dark">
                                                            <ins>$229.00</ins>
                                                        </span>
                                                    <span class="star-rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <span class="review">5 Review(s)</span>
                                                        </span>
                                                    <div class="group-btn-hover">
                                                        <div class="inner">
                                                            <a href="compare.html" class="compare"><i
                                                                    class="fa fa-exchange"></i></a>
                                                            <a href="#" class="add-to-cart">Add to cart</a>
                                                            <a href="wishlist.html" class="wishlist"><i
                                                                    class="fa fa-heart-o"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="owl-one-row">
                                        <div class="product-item style1">
                                            <div class="product-inner equal-elem">
                                                <div class="product-thumb">
                                                    <div class="thumb-inner">
                                                        <a href="#"><img src="assets/images/home2/p3.jpg" alt="p3"></a>
                                                    </div>
                                                    <span class="onnew">new</span>
                                                    <a href="#" class="quick-view">Quick View</a>
                                                </div>
                                                <div class="product-innfo">
                                                    <div class="product-name"><a href="#">Smartphone 4 GB
                                                        </a></div>
                                                    <span class="price price-dark">
                                                            <ins>$229.00</ins>
                                                        </span>
                                                    <span class="star-rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <span class="review">5 Review(s)</span>
                                                        </span>
                                                    <div class="group-btn-hover">
                                                        <div class="inner">
                                                            <a href="compare.html" class="compare"><i
                                                                    class="fa fa-exchange"></i></a>
                                                            <a href="#" class="add-to-cart">Add to cart</a>
                                                            <a href="wishlist.html" class="wishlist"><i
                                                                    class="fa fa-heart-o"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-item style1">
                                            <div class="product-inner equal-elem">
                                                <div class="product-thumb">
                                                    <div class="thumb-inner">
                                                        <a href="#"><img src="assets/images/home2/p9.jpg" alt="p9"></a>
                                                    </div>
                                                    <a href="#" class="quick-view">Quick View</a>
                                                </div>
                                                <div class="product-innfo">
                                                    <div class="product-name"><a href="#">Smartphone New
                                                        </a></div>
                                                    <span class="price price-dark">
                                                            <ins>$229.00</ins>
                                                        </span>
                                                    <span class="star-rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <span class="review">5 Review(s)</span>
                                                        </span>
                                                    <div class="group-btn-hover">
                                                        <div class="inner">
                                                            <a href="compare.html" class="compare"><i
                                                                    class="fa fa-exchange"></i></a>
                                                            <a href="#" class="add-to-cart">Add to cart</a>
                                                            <a href="wishlist.html" class="wishlist"><i
                                                                    class="fa fa-heart-o"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="owl-one-row">
                                        <div class="product-item style1">
                                            <div class="product-inner equal-elem">
                                                <div class="product-thumb">
                                                    <div class="thumb-inner">
                                                        <a href="#"><img src="assets/images/home2/p5.jpg" alt="p5"></a>
                                                    </div>
                                                    <a href="#" class="quick-view">Quick View</a>
                                                </div>
                                                <div class="product-innfo">
                                                    <div class="product-name"><a href="#">Notebook Pro
                                                        </a></div>
                                                    <span class="price price-dark">
                                                            <ins>$229.00</ins>
                                                        </span>
                                                    <span class="star-rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <span class="review">5 Review(s)</span>
                                                        </span>
                                                    <div class="group-btn-hover">
                                                        <div class="inner">
                                                            <a href="compare.html" class="compare"><i
                                                                    class="fa fa-exchange"></i></a>
                                                            <a href="#" class="add-to-cart">Add to cart</a>
                                                            <a href="wishlist.html" class="wishlist"><i
                                                                    class="fa fa-heart-o"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-item style1">
                                            <div class="product-inner equal-elem">
                                                <div class="product-thumb">
                                                    <div class="thumb-inner">
                                                        <a href="#"><img src="assets/images/home2/p10.jpg"
                                                                         alt="p10"></a>
                                                    </div>
                                                    <span class="onsale">-50%</span>
                                                    <a href="#" class="quick-view">Quick View</a>
                                                </div>
                                                <div class="product-innfo">
                                                    <div class="product-name"><a href="#">Notebook Pro
                                                        </a></div>
                                                    <span class="price">
                                                            <ins>$229.00</ins>
                                                            <del>$259.00</del>
                                                        </span>
                                                    <span class="star-rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <span class="review">5 Review(s)</span>
                                                        </span>
                                                    <div class="group-btn-hover">
                                                        <div class="inner">
                                                            <a href="compare.html" class="compare"><i
                                                                    class="fa fa-exchange"></i></a>
                                                            <a href="#" class="add-to-cart">Add to cart</a>
                                                            <a href="wishlist.html" class="wishlist"><i
                                                                    class="fa fa-heart-o"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-3" class="tab-panel">
                                <div class="owl-carousel nav-style2 border-background equal-container" data-nav="false"
                                     data-autoplay="false" data-dots="false" data-loop="true" data-margin="30"
                                     data-responsive='{"0":{"items":1},"480":{"items":2},"768":{"items":3},"992":{"items":4},"1200":{"items":4}}'>
                                    <div class="owl-one-row">
                                        <div class="product-item style1">
                                            <div class="product-inner equal-elem">
                                                <div class="product-thumb">
                                                    <div class="thumb-inner">
                                                        <a href="#"><img src="assets/images/home2/p3.jpg" alt="p3"></a>
                                                    </div>
                                                    <span class="onnew">new</span>
                                                    <a href="#" class="quick-view">Quick View</a>
                                                </div>
                                                <div class="product-innfo">
                                                    <div class="product-name"><a href="#">Smartphone 4 GB
                                                        </a></div>
                                                    <span class="price price-dark">
                                                            <ins>$229.00</ins>
                                                        </span>
                                                    <span class="star-rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <span class="review">5 Review(s)</span>
                                                        </span>
                                                    <div class="group-btn-hover">
                                                        <div class="inner">
                                                            <a href="compare.html" class="compare"><i
                                                                    class="fa fa-exchange"></i></a>
                                                            <a href="#" class="add-to-cart">Add to cart</a>
                                                            <a href="wishlist.html" class="wishlist"><i
                                                                    class="fa fa-heart-o"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-item style1">
                                            <div class="product-inner equal-elem">
                                                <div class="product-thumb">
                                                    <div class="thumb-inner">
                                                        <a href="#"><img src="assets/images/home2/p2.jpg" alt="p2"></a>
                                                    </div>
                                                    <span class="onsale">-50%</span>
                                                    <a href="#" class="quick-view">Quick View</a>
                                                </div>
                                                <div class="product-innfo">
                                                    <div class="product-name"><a href="#">Photo Camera
                                                        </a></div>
                                                    <span class="price">
                                                            <ins>$229.00</ins>
                                                            <del>$259.00</del>
                                                        </span>
                                                    <span class="star-rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <span class="review">5 Review(s)</span>
                                                        </span>
                                                    <div class="group-btn-hover">
                                                        <div class="inner">
                                                            <a href="compare.html" class="compare"><i
                                                                    class="fa fa-exchange"></i></a>
                                                            <a href="#" class="add-to-cart">Add to cart</a>
                                                            <a href="wishlist.html" class="wishlist"><i
                                                                    class="fa fa-heart-o"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="owl-one-row">
                                        <div class="product-item style1">
                                            <div class="product-inner equal-elem">
                                                <div class="product-thumb">
                                                    <div class="thumb-inner">
                                                        <a href="#"><img src="assets/images/home2/p10.jpg"
                                                                         alt="p10"></a>
                                                    </div>
                                                    <span class="onsale">-50%</span>
                                                    <a href="#" class="quick-view">Quick View</a>
                                                </div>
                                                <div class="product-innfo">
                                                    <div class="product-name"><a href="#">Notebook Pro
                                                        </a></div>
                                                    <span class="price">
                                                            <ins>$229.00</ins>
                                                            <del>$259.00</del>
                                                        </span>
                                                    <span class="star-rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <span class="review">5 Review(s)</span>
                                                        </span>
                                                    <div class="group-btn-hover">
                                                        <div class="inner">
                                                            <a href="compare.html" class="compare"><i
                                                                    class="fa fa-exchange"></i></a>
                                                            <a href="#" class="add-to-cart">Add to cart</a>
                                                            <a href="wishlist.html" class="wishlist"><i
                                                                    class="fa fa-heart-o"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-item style1">
                                            <div class="product-inner equal-elem">
                                                <div class="product-thumb">
                                                    <div class="thumb-inner">
                                                        <a href="#"><img src="assets/images/home2/p1.jpg" alt="p1"></a>
                                                    </div>
                                                    <span class="onsale">-50%</span>
                                                    <a href="#" class="quick-view">Quick View</a>
                                                </div>
                                                <div class="product-innfo">
                                                    <div class="product-name"><a href="#">Game Controller
                                                        </a></div>
                                                    <span class="price">
                                                            <ins>$229.00</ins>
                                                            <del>$259.00</del>
                                                        </span>
                                                    <span class="star-rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <span class="review">5 Review(s)</span>
                                                        </span>
                                                    <div class="group-btn-hover">
                                                        <div class="inner">
                                                            <a href="compare.html" class="compare"><i
                                                                    class="fa fa-exchange"></i></a>
                                                            <a href="#" class="add-to-cart">Add to cart</a>
                                                            <a href="wishlist.html" class="wishlist"><i
                                                                    class="fa fa-heart-o"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="owl-one-row">
                                        <div class="product-item style1">
                                            <div class="product-inner equal-elem">
                                                <div class="product-thumb">
                                                    <div class="thumb-inner">
                                                        <a href="#"><img src="assets/images/home2/p8.jpg" alt="p8"></a>
                                                    </div>
                                                    <span class="onsale">-50%</span>
                                                    <a href="#" class="quick-view">Quick View</a>
                                                </div>
                                                <div class="product-innfo">
                                                    <div class="product-name"><a href="#">Retina Laptop
                                                        </a></div>
                                                    <span class="price">
                                                            <ins>$229.00</ins>
                                                            <del>$259.00</del>
                                                        </span>
                                                    <span class="star-rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <span class="review">5 Review(s)</span>
                                                        </span>
                                                    <div class="group-btn-hover">
                                                        <div class="inner">
                                                            <a href="compare.html" class="compare"><i
                                                                    class="fa fa-exchange"></i></a>
                                                            <a href="#" class="add-to-cart">Add to cart</a>
                                                            <a href="wishlist.html" class="wishlist"><i
                                                                    class="fa fa-heart-o"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-item style1">
                                            <div class="product-inner equal-elem">
                                                <div class="product-thumb">
                                                    <div class="thumb-inner">
                                                        <a href="#"><img src="assets/images/home2/p4.jpg" alt="p4"></a>
                                                    </div>
                                                    <a href="#" class="quick-view">Quick View</a>
                                                </div>
                                                <div class="product-innfo">
                                                    <div class="product-name"><a href="#">Smartphone New
                                                        </a></div>
                                                    <span class="price price-dark">
                                                            <ins>$229.00</ins>
                                                        </span>
                                                    <span class="star-rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <span class="review">5 Review(s)</span>
                                                        </span>
                                                    <div class="group-btn-hover">
                                                        <div class="inner">
                                                            <a href="compare.html" class="compare"><i
                                                                    class="fa fa-exchange"></i></a>
                                                            <a href="#" class="add-to-cart">Add to cart</a>
                                                            <a href="wishlist.html" class="wishlist"><i
                                                                    class="fa fa-heart-o"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="owl-one-row">
                                        <div class="product-item style1">
                                            <div class="product-inner equal-elem">
                                                <div class="product-thumb">
                                                    <div class="thumb-inner">
                                                        <a href="#"><img src="assets/images/home2/p6.jpg" alt="p6"></a>
                                                    </div>
                                                    <span class="onsale">-50%</span>
                                                    <a href="#" class="quick-view">Quick View</a>
                                                </div>
                                                <div class="product-innfo">
                                                    <div class="product-name"><a href="#">Retina Laptop
                                                        </a></div>
                                                    <span class="price price-dark">
                                                            <ins>$229.00</ins>
                                                        </span>
                                                    <span class="star-rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <span class="review">5 Review(s)</span>
                                                        </span>
                                                    <div class="group-btn-hover">
                                                        <div class="inner">
                                                            <a href="compare.html" class="compare"><i
                                                                    class="fa fa-exchange"></i></a>
                                                            <a href="#" class="add-to-cart">Add to cart</a>
                                                            <a href="wishlist.html" class="wishlist"><i
                                                                    class="fa fa-heart-o"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-item style1">
                                            <div class="product-inner equal-elem">
                                                <div class="product-thumb">
                                                    <div class="thumb-inner">
                                                        <a href="#"><img src="assets/images/home2/p9.jpg" alt="p9"></a>
                                                    </div>
                                                    <a href="#" class="quick-view">Quick View</a>
                                                </div>
                                                <div class="product-innfo">
                                                    <div class="product-name"><a href="#">Smartphone New
                                                        </a></div>
                                                    <span class="price price-dark">
                                                            <ins>$229.00</ins>
                                                        </span>
                                                    <span class="star-rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <span class="review">5 Review(s)</span>
                                                        </span>
                                                    <div class="group-btn-hover">
                                                        <div class="inner">
                                                            <a href="compare.html" class="compare"><i
                                                                    class="fa fa-exchange"></i></a>
                                                            <a href="#" class="add-to-cart">Add to cart</a>
                                                            <a href="wishlist.html" class="wishlist"><i
                                                                    class="fa fa-heart-o"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="owl-one-row">
                                        <div class="product-item style1">
                                            <div class="product-inner equal-elem">
                                                <div class="product-thumb">
                                                    <div class="thumb-inner">
                                                        <a href="#"><img src="assets/images/home2/p5.jpg" alt="p5"></a>
                                                    </div>
                                                    <a href="#" class="quick-view">Quick View</a>
                                                </div>
                                                <div class="product-innfo">
                                                    <div class="product-name"><a href="#">Notebook Pro
                                                        </a></div>
                                                    <span class="price price-dark">
                                                            <ins>$229.00</ins>
                                                        </span>
                                                    <span class="star-rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <span class="review">5 Review(s)</span>
                                                        </span>
                                                    <div class="group-btn-hover">
                                                        <div class="inner">
                                                            <a href="compare.html" class="compare"><i
                                                                    class="fa fa-exchange"></i></a>
                                                            <a href="#" class="add-to-cart">Add to cart</a>
                                                            <a href="wishlist.html" class="wishlist"><i
                                                                    class="fa fa-heart-o"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-item style1">
                                            <div class="product-inner equal-elem">
                                                <div class="product-thumb">
                                                    <div class="thumb-inner">
                                                        <a href="#"><img src="assets/images/home2/p7.jpg" alt="p7"></a>
                                                    </div>
                                                    <a href="#" class="quick-view">Quick View</a>
                                                </div>
                                                <div class="product-innfo">
                                                    <div class="product-name"><a href="#">Ring Video Doorbell
                                                        </a></div>
                                                    <span class="price price-dark">
                                                            <ins>$229.00</ins>
                                                        </span>
                                                    <span class="star-rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <span class="review">5 Review(s)</span>
                                                        </span>
                                                    <div class="group-btn-hover">
                                                        <div class="inner">
                                                            <a href="compare.html" class="compare"><i
                                                                    class="fa fa-exchange"></i></a>
                                                            <a href="#" class="add-to-cart">Add to cart</a>
                                                            <a href="wishlist.html" class="wishlist"><i
                                                                    class="fa fa-heart-o"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-4" class="tab-panel">
                                <div class="owl-carousel nav-style2 border-background equal-container" data-nav="false"
                                     data-autoplay="false" data-dots="false" data-loop="true" data-margin="30"
                                     data-responsive='{"0":{"items":1},"480":{"items":2},"768":{"items":3},"992":{"items":4},"1200":{"items":4}}'>
                                    <div class="owl-one-row">
                                        <div class="product-item style1">
                                            <div class="product-inner equal-elem">
                                                <div class="product-thumb">
                                                    <div class="thumb-inner">
                                                        <a href="#"><img src="assets/images/home2/p2.jpg" alt="p2"></a>
                                                    </div>
                                                    <span class="onsale">-50%</span>
                                                    <a href="#" class="quick-view">Quick View</a>
                                                </div>
                                                <div class="product-innfo">
                                                    <div class="product-name"><a href="#">Photo Camera
                                                        </a></div>
                                                    <span class="price">
                                                            <ins>$229.00</ins>
                                                            <del>$259.00</del>
                                                        </span>
                                                    <span class="star-rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <span class="review">5 Review(s)</span>
                                                        </span>
                                                    <div class="group-btn-hover">
                                                        <div class="inner">
                                                            <a href="compare.html" class="compare"><i
                                                                    class="fa fa-exchange"></i></a>
                                                            <a href="#" class="add-to-cart">Add to cart</a>
                                                            <a href="wishlist.html" class="wishlist"><i
                                                                    class="fa fa-heart-o"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-item style1">
                                            <div class="product-inner equal-elem">
                                                <div class="product-thumb">
                                                    <div class="thumb-inner">
                                                        <a href="#"><img src="assets/images/home2/p6.jpg" alt="p6"></a>
                                                    </div>
                                                    <span class="onsale">-50%</span>
                                                    <a href="#" class="quick-view">Quick View</a>
                                                </div>
                                                <div class="product-innfo">
                                                    <div class="product-name"><a href="#">Retina Laptop
                                                        </a></div>
                                                    <span class="price price-dark">
                                                            <ins>$229.00</ins>
                                                        </span>
                                                    <span class="star-rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <span class="review">5 Review(s)</span>
                                                        </span>
                                                    <div class="group-btn-hover">
                                                        <div class="inner">
                                                            <a href="compare.html" class="compare"><i
                                                                    class="fa fa-exchange"></i></a>
                                                            <a href="#" class="add-to-cart">Add to cart</a>
                                                            <a href="wishlist.html" class="wishlist"><i
                                                                    class="fa fa-heart-o"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="owl-one-row">
                                        <div class="product-item style1">
                                            <div class="product-inner equal-elem">
                                                <div class="product-thumb">
                                                    <div class="thumb-inner">
                                                        <a href="#"><img src="assets/images/home2/p1.jpg" alt="p1"></a>
                                                    </div>
                                                    <span class="onsale">-50%</span>
                                                    <a href="#" class="quick-view">Quick View</a>
                                                </div>
                                                <div class="product-innfo">
                                                    <div class="product-name"><a href="#">Game Controller
                                                        </a></div>
                                                    <span class="price">
                                                            <ins>$229.00</ins>
                                                            <del>$259.00</del>
                                                        </span>
                                                    <span class="star-rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <span class="review">5 Review(s)</span>
                                                        </span>
                                                    <div class="group-btn-hover">
                                                        <div class="inner">
                                                            <a href="compare.html" class="compare"><i
                                                                    class="fa fa-exchange"></i></a>
                                                            <a href="#" class="add-to-cart">Add to cart</a>
                                                            <a href="wishlist.html" class="wishlist"><i
                                                                    class="fa fa-heart-o"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-item style1">
                                            <div class="product-inner equal-elem">
                                                <div class="product-thumb">
                                                    <div class="thumb-inner">
                                                        <a href="#"><img src="assets/images/home2/p7.jpg" alt="p7"></a>
                                                    </div>
                                                    <a href="#" class="quick-view">Quick View</a>
                                                </div>
                                                <div class="product-innfo">
                                                    <div class="product-name"><a href="#">Ring Video Doorbell
                                                        </a></div>
                                                    <span class="price price-dark">
                                                            <ins>$229.00</ins>
                                                        </span>
                                                    <span class="star-rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <span class="review">5 Review(s)</span>
                                                        </span>
                                                    <div class="group-btn-hover">
                                                        <div class="inner">
                                                            <a href="compare.html" class="compare"><i
                                                                    class="fa fa-exchange"></i></a>
                                                            <a href="#" class="add-to-cart">Add to cart</a>
                                                            <a href="wishlist.html" class="wishlist"><i
                                                                    class="fa fa-heart-o"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="owl-one-row">
                                        <div class="product-item style1">
                                            <div class="product-inner equal-elem">
                                                <div class="product-thumb">
                                                    <div class="thumb-inner">
                                                        <a href="#"><img src="assets/images/home2/p4.jpg" alt="p4"></a>
                                                    </div>
                                                    <a href="#" class="quick-view">Quick View</a>
                                                </div>
                                                <div class="product-innfo">
                                                    <div class="product-name"><a href="#">Smartphone New
                                                        </a></div>
                                                    <span class="price price-dark">
                                                            <ins>$229.00</ins>
                                                        </span>
                                                    <span class="star-rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <span class="review">5 Review(s)</span>
                                                        </span>
                                                    <div class="group-btn-hover">
                                                        <div class="inner">
                                                            <a href="compare.html" class="compare"><i
                                                                    class="fa fa-exchange"></i></a>
                                                            <a href="#" class="add-to-cart">Add to cart</a>
                                                            <a href="wishlist.html" class="wishlist"><i
                                                                    class="fa fa-heart-o"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-item style1">
                                            <div class="product-inner equal-elem">
                                                <div class="product-thumb">
                                                    <div class="thumb-inner">
                                                        <a href="#"><img src="assets/images/home2/p3.jpg" alt="p3"></a>
                                                    </div>
                                                    <span class="onnew">new</span>
                                                    <a href="#" class="quick-view">Quick View</a>
                                                </div>
                                                <div class="product-innfo">
                                                    <div class="product-name"><a href="#">Smartphone 4 GB
                                                        </a></div>
                                                    <span class="price price-dark">
                                                            <ins>$229.00</ins>
                                                        </span>
                                                    <span class="star-rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <span class="review">5 Review(s)</span>
                                                        </span>
                                                    <div class="group-btn-hover">
                                                        <div class="inner">
                                                            <a href="compare.html" class="compare"><i
                                                                    class="fa fa-exchange"></i></a>
                                                            <a href="#" class="add-to-cart">Add to cart</a>
                                                            <a href="wishlist.html" class="wishlist"><i
                                                                    class="fa fa-heart-o"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="owl-one-row">
                                        <div class="product-item style1">
                                            <div class="product-inner equal-elem">
                                                <div class="product-thumb">
                                                    <div class="thumb-inner">
                                                        <a href="#"><img src="assets/images/home2/p8.jpg" alt="p8"></a>
                                                    </div>
                                                    <span class="onsale">-50%</span>
                                                    <a href="#" class="quick-view">Quick View</a>
                                                </div>
                                                <div class="product-innfo">
                                                    <div class="product-name"><a href="#">Retina Laptop
                                                        </a></div>
                                                    <span class="price">
                                                            <ins>$229.00</ins>
                                                            <del>$259.00</del>
                                                        </span>
                                                    <span class="star-rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <span class="review">5 Review(s)</span>
                                                        </span>
                                                    <div class="group-btn-hover">
                                                        <div class="inner">
                                                            <a href="compare.html" class="compare"><i
                                                                    class="fa fa-exchange"></i></a>
                                                            <a href="#" class="add-to-cart">Add to cart</a>
                                                            <a href="wishlist.html" class="wishlist"><i
                                                                    class="fa fa-heart-o"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-item style1">
                                            <div class="product-inner equal-elem">
                                                <div class="product-thumb">
                                                    <div class="thumb-inner">
                                                        <a href="#"><img src="assets/images/home2/p9.jpg" alt="p9"></a>
                                                    </div>
                                                    <a href="#" class="quick-view">Quick View</a>
                                                </div>
                                                <div class="product-innfo">
                                                    <div class="product-name"><a href="#">Smartphone New
                                                        </a></div>
                                                    <span class="price price-dark">
                                                            <ins>$229.00</ins>
                                                        </span>
                                                    <span class="star-rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <span class="review">5 Review(s)</span>
                                                        </span>
                                                    <div class="group-btn-hover">
                                                        <div class="inner">
                                                            <a href="compare.html" class="compare"><i
                                                                    class="fa fa-exchange"></i></a>
                                                            <a href="#" class="add-to-cart">Add to cart</a>
                                                            <a href="wishlist.html" class="wishlist"><i
                                                                    class="fa fa-heart-o"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="owl-one-row">
                                        <div class="product-item style1">
                                            <div class="product-inner equal-elem">
                                                <div class="product-thumb">
                                                    <div class="thumb-inner">
                                                        <a href="#"><img src="assets/images/home2/p5.jpg" alt="p5"></a>
                                                    </div>
                                                    <a href="#" class="quick-view">Quick View</a>
                                                </div>
                                                <div class="product-innfo">
                                                    <div class="product-name"><a href="#">Notebook Pro
                                                        </a></div>
                                                    <span class="price price-dark">
                                                            <ins>$229.00</ins>
                                                        </span>
                                                    <span class="star-rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <span class="review">5 Review(s)</span>
                                                        </span>
                                                    <div class="group-btn-hover">
                                                        <div class="inner">
                                                            <a href="compare.html" class="compare"><i
                                                                    class="fa fa-exchange"></i></a>
                                                            <a href="#" class="add-to-cart">Add to cart</a>
                                                            <a href="wishlist.html" class="wishlist"><i
                                                                    class="fa fa-heart-o"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-item style1">
                                            <div class="product-inner equal-elem">
                                                <div class="product-thumb">
                                                    <div class="thumb-inner">
                                                        <a href="#"><img src="assets/images/home2/p10.jpg"
                                                                         alt="p10"></a>
                                                    </div>
                                                    <span class="onsale">-50%</span>
                                                    <a href="#" class="quick-view">Quick View</a>
                                                </div>
                                                <div class="product-innfo">
                                                    <div class="product-name"><a href="#">Notebook Pro
                                                        </a></div>
                                                    <span class="price">
                                                            <ins>$229.00</ins>
                                                            <del>$259.00</del>
                                                        </span>
                                                    <span class="star-rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <span class="review">5 Review(s)</span>
                                                        </span>
                                                    <div class="group-btn-hover">
                                                        <div class="inner">
                                                            <a href="compare.html" class="compare"><i
                                                                    class="fa fa-exchange"></i></a>
                                                            <a href="#" class="add-to-cart">Add to cart</a>
                                                            <a href="wishlist.html" class="wishlist"><i
                                                                    class="fa fa-heart-o"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="block-section-6 page-product">
            <div class="container">
                <div class="promotion-banner style-3">
                    <a href="#" class="banner-img"><img src="assets/images/home2/banner-3.jpg" alt="banner-3"></a>
                    <div class="promotion-banner-inner">
                        <h4>Top Collection</h4>
                        <h3>All New Smart Watches</h3>
                        <a class="banner-link" href="grid-product.html">Shop now</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="block-the-blog">
            <div class="container">
                <div class="title-of-section"><span>From The Blog</span></div>
                <div class="owl-carousel nav-style2" data-nav="true" data-autoplay="false" data-dots="true"
                     data-loop="true" data-margin="30"
                     data-responsive='{"0":{"items":1},"480":{"items":2},"600":{"items":2},"992":{"items":3}}'>
                    <div class="blog-item">
                        <div class="post-thumb">
                            <a href="#"><img src="assets/images/home1/blog1.jpg" alt="blog1"></a>
                        </div>
                        <div class="post-item-info">
                            <h3 class="post-name"><a href="#">It’s all about the bread: whole grain home</a></h3>
                            <div class="post-metas">
                                <span class="author">Post by: <span>Admin</span></span>
                                <span class="comment"><i class="fa fa-comment" aria-hidden="true"></i>36 Comments</span>
                            </div>
                            <div>Lorem ipsum dolor sit amet consectetur adipiscing, elit integer urna placerat donec,
                                cum viverra egestas ac luctus...
                            </div>
                        </div>
                    </div>
                    <div class="blog-item">
                        <div class="post-thumb">
                            <a href="#"><img src="assets/images/home1/blog2.jpg" alt="blog2"></a>
                        </div>
                        <div class="post-item-info">
                            <h3 class="post-name"><a href="#">Ridiculus nascetur, malesuada non in netus.</a></h3>
                            <div class="post-metas">
                                <span class="author">Post by: <span>Admin</span></span>
                                <span class="comment"><i class="fa fa-comment" aria-hidden="true"></i>36 Comments</span>
                            </div>
                            <div>Lorem ipsum dolor sit amet consectetur adipiscing, elit integer urna placerat donec,
                                cum viverra egestas ac luctus...
                            </div>
                        </div>
                    </div>
                    <div class="blog-item">
                        <div class="post-thumb">
                            <a href="#"><img src="assets/images/home1/blog3.jpg" alt="blog3"></a>
                        </div>
                        <div class="post-item-info">
                            <h3 class="post-name"><a href="#">Using Lorem Ipsum allows designers to put</a></h3>
                            <div class="post-metas">
                                <span class="author">Post by: <span>Admin</span></span>
                                <span class="comment"><i class="fa fa-comment" aria-hidden="true"></i>36 Comments</span>
                            </div>
                            <div>Lorem ipsum dolor sit amet consectetur adipiscing, elit integer urna placerat donec,
                                cum viverra egestas ac luctus...
                            </div>
                        </div>
                    </div>
                    <div class="blog-item">
                        <div class="post-thumb">
                            <a href="#"><img src="assets/images/home1/blog1.jpg" alt="blog1"></a>
                        </div>
                        <div class="post-item-info">
                            <h3 class="post-name"><a href="#">It’s all about the bread: whole grain home</a></h3>
                            <div class="post-metas">
                                <span class="author">Post by: <span>Admin</span></span>
                                <span class="comment"><i class="fa fa-comment" aria-hidden="true"></i>36 Comments</span>
                            </div>
                            <div>Lorem ipsum dolor sit amet consectetur adipiscing, elit integer urna placerat donec,
                                cum viverra egestas ac luctus...
                            </div>
                        </div>
                    </div>
                    <div class="blog-item">
                        <div class="post-thumb">
                            <a href="#"><img src="assets/images/home1/blog2.jpg" alt="blog2"></a>
                        </div>
                        <div class="post-item-info">
                            <h3 class="post-name"><a href="#">Ridiculus nascetur, malesuada non in netus.</a></h3>
                            <div class="post-metas">
                                <span class="author">Post by: <span>Admin</span></span>
                                <span class="comment"><i class="fa fa-comment" aria-hidden="true"></i>36 Comments</span>
                            </div>
                            <div>Lorem ipsum dolor sit amet consectetur adipiscing, elit integer urna placerat donec,
                                cum viverra egestas ac luctus...
                            </div>
                        </div>
                    </div>
                    <div class="blog-item">
                        <div class="post-thumb">
                            <a href="#"><img src="assets/images/home1/blog3.jpg" alt="blog3"></a>
                        </div>
                        <div class="post-item-info">
                            <h3 class="post-name"><a href="#">Using Lorem Ipsum allows designers to put</a></h3>
                            <div class="post-metas">
                                <span class="author">Post by: <span>Admin</span></span>
                                <span class="comment"><i class="fa fa-comment" aria-hidden="true"></i>36 Comments</span>
                            </div>
                            <div>Lorem ipsum dolor sit amet consectetur adipiscing, elit integer urna placerat donec,
                                cum viverra egestas ac luctus...
                            </div>
                        </div>
                    </div>
                    <div class="blog-item">
                        <div class="post-thumb">
                            <a href="#"><img src="assets/images/home1/blog1.jpg" alt="blog1"></a>
                        </div>
                        <div class="post-item-info">
                            <h3 class="post-name"><a href="#">It’s all about the bread: whole grain home</a></h3>
                            <div class="post-metas">
                                <span class="author">Post by: <span>Admin</span></span>
                                <span class="comment"><i class="fa fa-comment" aria-hidden="true"></i>36 Comments</span>
                            </div>
                            <div>Lorem ipsum dolor sit amet consectetur adipiscing, elit integer urna placerat donec,
                                cum viverra egestas ac luctus...
                            </div>
                        </div>
                    </div>
                    <div class="blog-item">
                        <div class="post-thumb">
                            <a href="#"><img src="assets/images/home1/blog2.jpg" alt="blog2"></a>
                        </div>
                        <div class="post-item-info">
                            <h3 class="post-name"><a href="#">Ridiculus nascetur, malesuada non in netus.</a></h3>
                            <div class="post-metas">
                                <span class="author">Post by: <span>Admin</span></span>
                                <span class="comment"><i class="fa fa-comment" aria-hidden="true"></i>36 Comments</span>
                            </div>
                            <div>Lorem ipsum dolor sit amet consectetur adipiscing, elit integer urna placerat donec,
                                cum viverra egestas ac luctus...
                            </div>
                        </div>
                    </div>
                    <div class="blog-item">
                        <div class="post-thumb">
                            <a href="#"><img src="assets/images/home1/blog3.jpg" alt="blog3"></a>
                        </div>
                        <div class="post-item-info">
                            <h3 class="post-name"><a href="#">Using Lorem Ipsum allows designers to put</a></h3>
                            <div class="post-metas">
                                <span class="author">Post by: <span>Admin</span></span>
                                <span class="comment"><i class="fa fa-comment" aria-hidden="true"></i>36 Comments</span>
                            </div>
                            <div>Lorem ipsum dolor sit amet consectetur adipiscing, elit integer urna placerat donec,
                                cum viverra egestas ac luctus...
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection


