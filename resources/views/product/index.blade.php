@extends('layouts.app')

@section('content')
    <main class="site-main">
        <div class="container">
            <ol class="breadcrumb-page">
                <li><a href="index.html">Home </a></li>
                <li class="active"><a href="#">Detail</a></li>
            </ol>
        </div>
        <div class="container">
            <div class="product-content-single">
                <div class="row">
                    <div class="col-md-6 col-sm-12 padding-right">
                        <div class="product-media">
                            <div class="image-preview-container image-thick-box image_preview_container">
                                <img id="img_zoom" data-zoom-image="{{ asset('uploads/'.$product->image)  }}"
                                     src="{{ asset('uploads/'.$product->image) }}" alt="" style="width: 100%; height: 100%">
                                <a href="#" class="btn-zoom open_qv"><i class="flaticon-magnifying-glass"
                                                                        aria-hidden="true"></i></a>
                            </div>
                            <div class="product-preview image-small product_preview">
                                <div id="thumbnails" class="thumbnails_carousel owl-carousel nav-style4" data-nav="true"
                                     data-autoplay="false" data-dots="false" data-loop="true" data-margin="10"
                                     data-responsive='{"0":{"items":3},"480":{"items":5},"600":{"items":5},"1000":{"items":5}}'>
                                    <a href="#" data-image="{{ asset('assets/images/detail/thick-box-1.jpg') }}"
                                       data-zoom-image="{{ asset('assets/images/detail/thick-box-1.jpg') }}">
                                        <img src="{{ asset('assets/images/detail/i1.jpg') }}"
                                             data-large-image="{{ asset('assets/images/detail/thick-box-1.jpg') }}"
                                             alt="i1">
                                    </a>
                                    <a href="#" data-image="{{ asset('assets/images/detail/thick-box-1.jpg') }}"
                                       data-zoom-image="{{ asset('assets/images/detail/thick-box-1.jpg') }}">
                                        <img src="{{ asset('assets/images/detail/i2.jpg') }}"
                                             data-large-image="{{ asset('assets/images/detail/thick-box-1.jpg') }}"
                                             alt="i1">
                                    </a>
                                    <a href="#" data-image="{{ asset('assets/images/detail/thick-box-1.jpg') }}"
                                       data-zoom-image="{{ asset('}assets/images/detail/thick-box-1.jpg') }}">
                                        <img src="{{ asset('assets/images/detail/i11.jpg') }}"
                                             data-large-image="{{ asset('assets/images/detail/thick-box-1.jpg') }}"
                                             alt="i1">
                                    </a>
                                    <a href="#" data-image="{{ asset('assets/images/detail/thick-box-1.jpg') }}"
                                       data-zoom-image="{{ asset('assets/images/detail/thick-box-1.jpg') }}">
                                        <img src="{{ asset('assets/images/detail/i12.jpg') }}"
                                             data-large-image="{{ asset('assets/images/detail/thick-box-1.jpg') }}"
                                             alt="i1">
                                    </a>
                                    <a href="#" data-image="{{ asset('assets/images/detail/thick-box-1.jpg') }}"
                                       data-zoom-image="{{ asset('assets/images/detail/thick-box-1.jpg') }}">
                                        <img src="{{ asset('assets/images/detail/i13.jpg') }}"
                                             data-large-image="{{ asset('assets/images/detail/thick-box-1.jpg') }}"
                                             alt="i1">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="product-info-main">
                            <div class="product-name"><a href="#" class="bid-title">{{ $product->title }}</a></div>
                            <span class="star-rating">

                                    <i class="fa fa-star" aria-hidden="true"></i>

                                    <i class="fa fa-star" aria-hidden="true"></i>

                                    <i class="fa fa-star" aria-hidden="true"></i>

                                    <i class="fa fa-star" aria-hidden="true"></i>

                                    <i class="fa fa-star" aria-hidden="true"></i>

                                    <span class="review">5 Review(s)</span>

                                </span>
                            <div class="product-info-stock-sku">
                                <div class="stock available">
                                    <span class="label-stock">Availability: </span>In Stock
                                </div>
                            </div>
                            <div class="product-infomation">
                                <p>{!! $product->short_description !!}</p>
                            </div>
                            <div class="product-info-price">
                                <span class="price">
                                      <p>Bid Price</p>
                                    <ins class="bid-ins"
                                         data-price="{{$product->maxBid() ?? $product->start_price }}" >
                                        ${{ $product->maxBid() ?? $product->start_price}}
                                    </ins>
                                      <p>Buy Now Price</p>
                                    <ins>${{ $product->price }}</ins>
                                </span>
                                <div class="product-count-down">
                                    <div class="kt-countdown"
                                         data-y="{{ $product->auction_time['year'] }}"
                                         data-m="{{ $product->auction_time['month'] }}"
                                         data-d="{{ $product->auction_time['day'] }}"
                                         data-h="{{ $product->auction_time['hour'] }}"
                                         data-i="{{ $product->auction_time['minute'] }}"
                                         data-s="{{ $product->auction_time['second'] }}">
                                    </div>
                                </div>
                                <form id="bid-form">
                                    <div class="quantity">
                                        <h6 class="quantity-title">Stavka:</h6>
                                        <div class="buttons-added">
                                            <input type="text" value="{{ $product->start_price }}" title="Qty"
                                                   class="input-text qty text bid-input" size="3" name="bid">
                                            <a href="#" class="sign plus"><i class="fa fa-plus"></i></a>
                                            <a href="#" class="sign minus"><i class="fa fa-minus"></i></a>
                                            <input type="hidden" name="id" value="{{ $product->id }}">
                                        </div>
                                    </div>
                                    <div class="single-add-to-cart">
                                        <div class="row">
                                            <button type="button">Buy Now</button>
                                            <button type="submit" class="btn btn-info bid-btn">Bid Now</button>
                                        </div>
                                        <a href="compare.html" class="compare"><i class="fa fa-exchange"></i>Compare</a>
                                        <a href="wishlist.html" class="wishlist"><i class="fa fa-heart-o"
                                                                                    aria-hidden="true"></i>Wishlist</a>
                                    </div>
                                </form>
                            </div>
                            <div class="group-btn-share">
                                <a href="#"><img src="{{ asset('assets/images/detail/btn1.png') }}" alt="btn1"></a>
                                <a href="#"><img src="{{ asset('assets/images/detail/btn2.png') }}" alt="btn1"></a>
                                <a href="#"><img src="{{ asset('assets/images/detail/btn3.png') }}" alt="btn1"></a>
                                <a href="#"><img src="{{ asset('assets/images/detail/btn4.png') }}" alt="btn1"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="tab-details-product">
                <ul class="box-tab nav-tab">
                    <li class="active"><a data-toggle="tab" href="#tab-1">Description</a></li>
                    <li><a data-toggle="tab" href="#tab-2">Addtional Infomation</a></li>
                    <li><a data-toggle="tab" href="#tab-3">Reviews</a></li>
                </ul>
                <div class="tab-container">
                    <div id="tab-1" class="tab-panel active">
                        <div class="box-content">
                            <p>{!! $product->description !!}</p>
                        </div>
                    </div>
                    <div id="tab-2" class="tab-panel">
                        <div class="box-content">
                            <p>ipsum dolor sit amet, consectetur adipiscing elit. Vivamus non nulla ullamcorper,
                                interdum dolor vel, dictum justo. Vivamus finibus lorem id auctor
                                placerat. Ut fermentum nulla lectus, in laoreet metus ultrices ac. Integer eleifend urna
                                ultricies enim facilisis, vel fermentum eros porta.
                            </p>
                            <span>Weights & Dimensions</span>
                            <div class="parameter">
                                <p>Overall: 40" H x 35.5" L x 35.5" W</p>
                                <p>Bar height:40"</p>
                                <p>Overall Product Weight: 88 lbs</p>
                            </div>
                        </div>
                    </div>
                    <div id="tab-3" class="tab-panel">
                        <div class="box-content">
                            <form method="post" action="#" class="new-review-form">
                                <a href="#" class="form-title">Write a review</a>
                                <div class="form-content">
                                    <p class="form-row form-row-wide">
                                        <label>Name</label>
                                        <input type="text" value="" name="text" placeholder="Enter your name"
                                               class="input-text">
                                    </p>
                                    <p class="form-row form-row-wide">
                                        <label>Email</label>
                                        <input type="text" name="text" placeholder="admin@example.com"
                                               class="input-text">
                                    </p>
                                    <p class="form-row form-row-wide">
                                        <label>Review Title<span class="required">*</span></label>
                                        <input type="email" name="email" placeholder="Give your review a title"
                                               class="input-text">
                                    </p>
                                    <p class="form-row form-row-wide">
                                        <label>Body of Review (1500)</label>
                                        <textarea title="message" aria-invalid="false" class="textarea-control" rows="5"
                                                  cols="40"
                                                  name="message"></textarea>
                                    </p>
                                    <p class="form-row">
                                        <input type="submit" value="Submit Review" name="Submit" class="button-submit">
                                    </p>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="block-recent-view single">
            <div class="container">
                <div class="title-of-section">You may be also interested</div>
                <div class="owl-carousel nav-style2 border-background equal-container" data-nav="true"
                     data-autoplay="false" data-dots="false" data-loop="true" data-margin="30"
                     data-responsive='{"0":{"items":1},"480":{"items":2},"768":{"items":3},"992":{"items":4},"1200":{"items":4}}'>
                    <div class="product-item style1">
                        <div class="product-inner equal-elem">
                            <div class="product-thumb">
                                <div class="thumb-inner">
                                    <a href="#"><img src="{{ asset('assets/images/home1/r1.jpg') }}" alt="r1"></a>
                                </div>
                                <span class="onsale">-50%</span>
                                <a href="#" class="quick-view">Quick View</a>
                            </div>
                            <div class="product-innfo">
                                <div class="product-name"><a href="#">Modern Watches</a></div>
                                <span class="price">

                                        <ins>$229.00</ins>

                                        <del>$259.00</del>

                                    </span>
                                <span class="star-rating">

                                        <i class="fa fa-star" aria-hidden="true"></i>

                                        <i class="fa fa-star" aria-hidden="true"></i>

                                        <i class="fa fa-star" aria-hidden="true"></i>

                                        <i class="fa fa-star" aria-hidden="true"></i>

                                        <i class="fa fa-star" aria-hidden="true"></i>

                                        <span class="review">5 Review(s)</span>

                                    </span>
                                <div class="group-btn-hover style2">
                                    <a href="#" class="add-to-cart"><i class="flaticon-shopping-cart"
                                                                       aria-hidden="true"></i></a>
                                    <a href="compare.html" class="compare"><i class="fa fa-exchange"></i></a>
                                    <a href="wishlist.html" class="wishlist"><i class="fa fa-heart-o"
                                                                                aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="product-item style1">
                        <div class="product-inner equal-elem">
                            <div class="product-thumb">
                                <div class="thumb-inner">
                                    <a href="#"><img src="{{ asset('assets/images/home1/r2.jpg') }}" alt="r2"></a>
                                </div>
                                <span class="onnew">new</span>
                                <a href="#" class="quick-view">Quick View</a>
                            </div>
                            <div class="product-innfo">
                                <div class="product-name"><a href="#">Cellphone Factory</a></div>
                                <span class="price price-dark">

                                        <ins>$229.00</ins>

                                    </span>
                                <span class="star-rating">

                                        <i class="fa fa-star" aria-hidden="true"></i>

                                        <i class="fa fa-star" aria-hidden="true"></i>

                                        <i class="fa fa-star" aria-hidden="true"></i>

                                        <i class="fa fa-star" aria-hidden="true"></i>

                                        <i class="fa fa-star" aria-hidden="true"></i>

                                        <span class="review">5 Review(s)</span>

                                    </span>
                                <div class="group-btn-hover style2">
                                    <a href="#" class="add-to-cart"><i class="flaticon-shopping-cart"
                                                                       aria-hidden="true"></i></a>
                                    <a href="compare.html" class="compare"><i class="fa fa-exchange"></i></a>
                                    <a href="wishlist.html" class="wishlist"><i class="fa fa-heart-o"
                                                                                aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="product-item style1">
                        <div class="product-inner equal-elem">
                            <div class="product-thumb">
                                <div class="thumb-inner">
                                    <a href="#"><img src="{{ asset('assets/images/home1/r3.jpg') }}" alt="r3"></a>
                                </div>
                                <a href="#" class="quick-view">Quick View</a>
                            </div>
                            <div class="product-innfo">
                                <div class="product-name"><a href="#">Smartphone 4 GB</a></div>
                                <span class="price price-dark">

                                        <ins>$229.00</ins>

                                    </span>
                                <span class="star-rating">

                                        <i class="fa fa-star" aria-hidden="true"></i>

                                        <i class="fa fa-star" aria-hidden="true"></i>

                                        <i class="fa fa-star" aria-hidden="true"></i>

                                        <i class="fa fa-star" aria-hidden="true"></i>

                                        <i class="fa fa-star" aria-hidden="true"></i>

                                        <span class="review">5 Review(s)</span>

                                    </span>
                                <div class="group-btn-hover style2">
                                    <a href="#" class="add-to-cart"><i class="flaticon-shopping-cart"
                                                                       aria-hidden="true"></i></a>
                                    <a href="compare.html" class="compare"><i class="fa fa-exchange"></i></a>
                                    <a href="wishlist.html" class="wishlist"><i class="fa fa-heart-o"
                                                                                aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="product-item style1">
                        <div class="product-inner equal-elem">
                            <div class="product-thumb">
                                <div class="thumb-inner">
                                    <a href="#"><img src="{{ asset('assets/images/home1/r4.jpg') }}" alt="r4"></a>
                                </div>
                                <a href="#" class="quick-view">Quick View</a>
                            </div>
                            <div class="product-innfo">
                                <div class="product-name"><a href="#">Extra Bass On</a></div>
                                <span class="price price-dark">

                                        <ins>$229.00</ins>

                                    </span>
                                <span class="star-rating">

                                        <i class="fa fa-star" aria-hidden="true"></i>

                                        <i class="fa fa-star" aria-hidden="true"></i>

                                        <i class="fa fa-star" aria-hidden="true"></i>

                                        <i class="fa fa-star" aria-hidden="true"></i>

                                        <i class="fa fa-star" aria-hidden="true"></i>

                                        <span class="review">5 Review(s)</span>

                                    </span>
                                <div class="group-btn-hover style2">
                                    <a href="#" class="add-to-cart"><i class="flaticon-shopping-cart"
                                                                       aria-hidden="true"></i></a>
                                    <a href="compare.html" class="compare"><i class="fa fa-exchange"></i></a>
                                    <a href="wishlist.html" class="wishlist"><i class="fa fa-heart-o"
                                                                                aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="product-item style1">
                        <div class="product-inner equal-elem">
                            <div class="product-thumb">
                                <div class="thumb-inner">
                                    <a href="#"><img src="{{ asset('assets/images/home1/r5.jpg') }}" alt="r5"></a>
                                </div>
                                <span class="onsale">-50%</span>
                                <a href="#" class="quick-view">Quick View</a>
                            </div>
                            <div class="product-innfo">
                                <div class="product-name"><a href="#">Smartwatch</a></div>
                                <span class="price">

                                        <ins>$229.00</ins>

                                        <del>$259.00</del>

                                    </span>
                                <span class="star-rating">

                                        <i class="fa fa-star" aria-hidden="true"></i>

                                        <i class="fa fa-star" aria-hidden="true"></i>

                                        <i class="fa fa-star" aria-hidden="true"></i>

                                        <i class="fa fa-star" aria-hidden="true"></i>

                                        <i class="fa fa-star" aria-hidden="true"></i>

                                        <span class="review">5 Review(s)</span>

                                    </span>
                                <div class="group-btn-hover style2">
                                    <a href="#" class="add-to-cart"><i class="flaticon-shopping-cart"
                                                                       aria-hidden="true"></i></a>
                                    <a href="compare.html" class="compare"><i class="fa fa-exchange"></i></a>
                                    <a href="wishlist.html" class="wishlist"><i class="fa fa-heart-o"
                                                                                aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="product-item style1">
                        <div class="product-inner equal-elem">
                            <div class="product-thumb">
                                <div class="thumb-inner">
                                    <a href="#"><img src="{{ asset('assets/images/home1/r6.jpg') }}" alt="r6"></a>
                                </div>
                                <a href="#" class="quick-view">Quick View</a>
                            </div>
                            <div class="product-innfo">
                                <div class="product-name"><a href="#">Modern Watches</a></div>
                                <span class="price price-dark">

                                        <ins>$229.00</ins>

                                    </span>
                                <span class="star-rating">

                                        <i class="fa fa-star" aria-hidden="true"></i>

                                        <i class="fa fa-star" aria-hidden="true"></i>

                                        <i class="fa fa-star" aria-hidden="true"></i>

                                        <i class="fa fa-star" aria-hidden="true"></i>

                                        <i class="fa fa-star" aria-hidden="true"></i>

                                        <span class="review">5 Review(s)</span>

                                    </span>
                                <div class="group-btn-hover style2">
                                    <a href="#" class="add-to-cart"><i class="flaticon-shopping-cart"
                                                                       aria-hidden="true"></i></a>
                                    <a href="compare.html" class="compare"><i class="fa fa-exchange"></i></a>
                                    <a href="wishlist.html" class="wishlist"><i class="fa fa-heart-o"
                                                                                aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

@endsection

@push('script')
    <script>
        $(function () {
            $('#bid-form').submit(function (e) {
                e.preventDefault();
                const $this = $(this);
                const form = new FormData($this[0]);
                let lastPrice = $('.bid-ins').data('price');
                if (+form.get('bid') > lastPrice) {
                    $.ajax({
                        url: '/product/store',
                        type: 'post',
                        data: form,
                        dataType: 'json',
                        processData: false,
                        contentType: false,
                        success: function (res) {
                           if(res.success){
                               $('.bid-ins').html('$' + form.get('bid')).data('price', form.get('bid'));
                           }
                        }
                    })
                }
            })
        })
    </script>
@endpush

